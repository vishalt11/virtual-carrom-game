//Carrom Game in OpenGl/Glut/C++ by Vishal Thamizharasan

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <sstream>  
#include <string> 
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif



using namespace std;

float tri_x = 0.0f;
float tri_y = -3.5f;
float tri_z = 0.0f;
float theta = 0.0f; 
float tri_h = 1.0f;
int flag_striker = 0;
int timer_s = 0;
int flag_initialize=0;

#define PI 3.141592653589
#define DEG2RAD(deg) (deg * PI / 180)

class board
{
public:
	char choice;
	int player;
	int score1, score2;
	void drawquad(float x, float y); 
	void drawScene(); 
	void createcircle(float rad); 
	void drawline(float a, float b, float c, float d); 
	void createcarmen(float p1, float p2); 
	void drawtriangle();
	void drawBox(float x, float y); 
};


class ball
{
	public:
		float ball_rad;
		float points1, points2; 
		float x, y, z; 
		float ball_velx, ball_vely;

		void set_params(float velx, float vely,float r=0, float p=0, float x_cor=0, float y_cor=0, float z_cor=0)
		{
			ball_rad=r;
			points1=points2=p;
			x=x_cor;
			y=y_cor;
			z=z_cor;
			ball_velx=velx;
			ball_vely=vely;
		}
		void drawcoins(); 
		void set_velx(float value)
		{
			ball_velx=value;
		}
		void set_vely(float value)
		{
			ball_vely=value;
		}

};

//Initializing the Board and Carrom men Objects
board B;
ball coins[7];
ball strike;
ball pocket1;
ball pocket2;
ball pocket3;
ball pocket4;


void ball::drawcoins() 
{
	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<360 ; i++) 
	{
		glVertex2f(ball_rad * cos(DEG2RAD(i)), ball_rad * sin(DEG2RAD(i)));
	}
	glEnd();
}

void board::drawquad(float x, float y){
	glBegin(GL_QUADS);
		glVertex2f(-x,-y);
		glVertex2f(x,-y);
		glVertex2f(x,y);
		glVertex2f(-x,y);
	glEnd();
}

void board::drawBox(float x, float y) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUADS);
    	glVertex2f(-x,-y);
    	glVertex2f(x,-y);
    	glVertex2f(x,y);
    	glVertex2f(-x,y);
    glEnd();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void board::createcircle(float rad)
{
	glBegin(GL_LINE_LOOP);
	for(int i=0 ; i<360 ; i++) 
	{
		glVertex2f(rad * cos(DEG2RAD(i)), rad * sin(DEG2RAD(i)));
	}
	glEnd();
}

void board::drawline(float a, float b, float c, float d)
{
	glBegin(GL_LINES);
	glVertex2f(a,b);
	glVertex2f(c,d);
	glEnd();

}

void board::drawtriangle() 
{
    glBegin(GL_TRIANGLES);
    	glColor3f(1.0f, 0.0f, 0.0f);
    	glVertex3f(0.0f, tri_h, 0.0f);
    	glVertex3f(-0.3f, -0.3f, 0.0f);
    	glVertex3f(0.3f, -0.3f, 0.0f);
    glEnd();
}

void renderBitmapString(float x,float y,void *font,char *string) 
{
	char *c;
	glRasterPos2d(x, y);
	for (c=string; *c != '\0'; c++) 
	{
		glutBitmapCharacter(font, *c);
	}
}

void board::drawScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -25.0f);

	// The outer-carrom-board
	glColor3f(0.36,0.254,0.13);
	drawquad(6,6);
	// The inner-carrom-board
	glColor3f(0.937,0.913,0.839);
	drawquad(5,5);


    //scoreboard outer-board
    glPushMatrix();
		glTranslatef(-11.0f,-3.0f,0.0f);
		glColor3f(1,0.904,0.729);
		drawquad(4,3);
	glPopMatrix();
	
    glPushMatrix();
		glTranslatef(-11.0f,-3.0f,0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		drawBox(4,3);
	glPopMatrix();
	//----------------------------------
	
	//----Instruction Box----------------
    glPushMatrix();
		glTranslatef(-11.0f,3.0f,0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		drawBox(4,3);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-11.0f,3.0f,0.0f);
		glColor3f(0.38,0.282,0.219);
		drawquad(4,3);
	glPopMatrix();
	//-----------------------------------

	//scoreboard inner-boards
	glPushMatrix();
		glTranslatef(-11.0f,4.5f,0.0f);
		glColor3f(1,0.904,0.729);
		drawquad(3.5,1);
	glPopMatrix();

    glPushMatrix();
		glTranslatef(-11.0f,1.5f,0.0f);
		glColor3f(1,0.904,0.729);
		drawquad(3.5,1);
	glPopMatrix();
	//----------------------------------

	//------------Score Board Text Generator----------------------
	glColor3f(0.0,0.0,0.0);
	char player1[]="PLAYER 1";
	renderBitmapString(-13.0f,4.8f,GLUT_BITMAP_HELVETICA_18,player1);

	std::ostringstream score1; 
	score1 << B.score1; 
	std::string buffer1 = score1.str(); 

	glRasterPos2d(-10.0f,4.8f);
	for(int i = 0; buffer1[i] != '\0'; i++)
		glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18 , buffer1[i]);

	char strpl[]="PLAYER 2";
	renderBitmapString(-13.0f,4.0f,GLUT_BITMAP_HELVETICA_18,strpl);

	std::ostringstream ostrpl; 
	ostrpl << B.score2; 
	std::string bufferpl = ostrpl.str(); 

	glRasterPos2d(-10.0f,4.0f);
	for(int l = 0; bufferpl[l] != '\0'; l++)
		glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18 , bufferpl[l]);

	glColor3f(0.0,0.0,0.0);
	char str2[]="TIMER(s)";
	renderBitmapString(-13.0f,1.3f,GLUT_BITMAP_HELVETICA_18,str2);

	std::ostringstream ostr; 
    ostr << timer_s; 
    std::string buffer = ostr.str(); 
    glRasterPos2d(-10.0f,1.3f);
   	for(int k = 0; buffer[k] != '\0'; k++)
    	glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18 , buffer[k]);
    //---------------------------------------------------------------

	//----------Instructions Text Generator------------------------
	char rule[]="Instructions";
	renderBitmapString(-12.5f,-0.7f,GLUT_BITMAP_HELVETICA_18,rule);
	char rule1[]="1. press 'w'(white coins) or 'b'";
	renderBitmapString(-14.5f,-2.0f,GLUT_BITMAP_8_BY_13,rule1);
	char rule11[]="(black coins) to start the game.";
    renderBitmapString(-14.1f,-2.5f,GLUT_BITMAP_8_BY_13,rule11);
   	char rule4[]="2. press 'enter' once your turn";
    renderBitmapString(-14.5f,-4.0f,GLUT_BITMAP_8_BY_13,rule4);
   	char rule41[]="is finished";
    renderBitmapString(-14.1f,-4.5f,GLUT_BITMAP_8_BY_13,rule41);
    //--------------------------------------------------------------

    //--------For the curves at the corners next to pocket----------
	glPushMatrix();
		glTranslatef(pocket1.x - 0.4, pocket1.y + 0.4, pocket1.z);
		glColor3f(0.38,0.282,0.219);
		drawquad(0.5,0.5);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(pocket2.x + 0.4, pocket2.y + 0.4, pocket2.z);
		glColor3f(0.38,0.282,0.219);
		drawquad(0.5,0.5);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(pocket3.x + 0.4, pocket3.y - 0.4, pocket3.z);
		glColor3f(0.38,0.282,0.219);
		drawquad(0.5,0.5);
	glPopMatrix();
	glPushMatrix();
		glTranslatef(pocket4.x - 0.4, pocket4.y - 0.4, pocket4.z);
		glColor3f(0.38,0.282,0.219);
		drawquad(0.5,0.5);
	glPopMatrix();
	//--------------------------------------------------------------
	//----------------pockets--------------------------
	glPushMatrix(); 
		glTranslatef(pocket1.x, pocket1.y, pocket1.z);
		glColor3f(0.082,0.031,0.0078);
		pocket1.drawcoins();
	glPopMatrix();

	glPushMatrix(); 
		glTranslatef(pocket2.x, pocket2.y, pocket2.z);
		glColor3f(0.082,0.031,0.0078);
		pocket2.drawcoins();
	glPopMatrix();

	glPushMatrix(); 
		glTranslatef(pocket3.x, pocket3.y, pocket3.z);
		glColor3f(0.082,0.031,0.0078);
		pocket3.drawcoins();
	glPopMatrix();

	glPushMatrix(); 
		glTranslatef(pocket4.x, pocket4.y, pocket4.z);
		glColor3f(0.082,0.031,0.0078);
		pocket4.drawcoins();
	glPopMatrix();
	//--------------------------------------------------

	//inner-bigcircle
	glPushMatrix(); 
		glTranslatef(0.0f, 0.0f, 0.0f);
		glColor3f(0.8,0.0,0.0);
		createcircle (1.2f);
	glPopMatrix();

	//left-top
	glColor3f(0.8,0.0,0.0);
	drawline(-3.8,3.5,-3.8,-3.5);
	glPushMatrix(); 
		glTranslatef(-3.5f, 3.5f, 0.0f);
		createcircle(0.3f);
	glPopMatrix();

	//left-bottom
	drawline(-3.5,3.8,3.5,3.8);
	glPushMatrix(); 
		glTranslatef(3.5f, 3.5f, 0.0f);
		createcircle(0.3f);
	glPopMatrix();

	//right-bottom
	drawline(3.8,3.5,3.8,-3.5);
	glPushMatrix(); 
		glTranslatef(3.5f,-3.5f, 0.0f);
		createcircle(0.3f);
	glPopMatrix();

	//right-top
	drawline(3.5,-3.8,-3.5,-3.8);
	glPushMatrix(); 
		glTranslatef(-3.5f,-3.5f, 0.0f);
		createcircle(0.3f);
	glPopMatrix();

	//once a choice is chosen, draw the coins and assign corresponding point values
	//for the player1 and player2
	if(choice == 'w')
		createcarmen(10.0, -5.0);
	else if(choice == 'b')	
		createcarmen(-5.0, 10.0);

	//Striker's power
	glPushMatrix();
    	glTranslatef(tri_x, tri_y, tri_z);
    	glRotatef(theta, 0.0f, 0.0f, 1.0f);
    	glScalef(0.2f, 0.2f, 0.2f);
    	drawtriangle();
    glPopMatrix();

	glPopMatrix();

	glutSwapBuffers();
}

void board::createcarmen(float p1, float p2){

	//Queen
	glPushMatrix(); 
		glTranslatef(coins[0].x,coins[0].y,coins[0].z);
		glColor3f(0.8,0.0,0.0);
		coins[0].drawcoins();
	glPopMatrix();

	//Striker
	glPushMatrix(); 
		glTranslatef(strike.x,strike.y,strike.z);
		glColor3f(0.401f, 0.619f, 0.737f);
		strike.drawcoins();
	glPopMatrix();
	strike.points1=strike.points2=-5;

	//Assign the corresponding Point values according to the player's choice
	if(coins[1].points1==0 )
	{
		coins[1].points1=p1;
		coins[1].points2=p2;
	}
	//White coins
	glPushMatrix(); 
		glTranslatef(coins[1].x,coins[1].y,coins[1].z);
		glColor3f(0.96f,0.77f, 0.643f);
		coins[1].drawcoins();
	glPopMatrix();
	if(coins[2].points1==0)
	{
		coins[2].points1=p1;
		coins[2].points2=p2;
	}
	glPushMatrix(); 
		glTranslatef(coins[2].x,coins[2].y,coins[2].z);
		glColor3f(0.96f,0.77f, 0.643f);
		coins[2].drawcoins();
	glPopMatrix();
	if(coins[3].points1==0)
	{
		coins[3].points1=p1;
		coins[3].points2=p2;
	}
	glPushMatrix(); 
		glTranslatef(coins[3].x,coins[3].y, coins[3].z);
		glColor3f(0.96f,0.77f, 0.643f);
		coins[3].drawcoins();
	glPopMatrix();

	//Assign the corresponding Point values according to the player's choice
	if(coins[4].points1==0)
	{
		coins[4].points1=p2;
		coins[4].points2=p1;
	}
	//BlacK coins
	glPushMatrix(); 
		glTranslatef(coins[4].x,coins[4].y,coins[4].z);
		glColor3f(0.0,0.0,0.0);
		coins[4].drawcoins();
	glPopMatrix();
	if(coins[5].points1==0)
	{
		coins[5].points1=p2;
		coins[5].points2=p1;
	}
	glPushMatrix(); 
		glTranslatef(coins[5].x,coins[5].y,coins[5].z);
		glColor3f(0.0,0.0,0.0);
		coins[5].drawcoins();
	glPopMatrix();
	if(coins[6].points1==0)
	{
		coins[6].points1=p2;
		coins[6].points2=p1;
	}
	glPushMatrix(); 
		glTranslatef(coins[6].x,coins[6].y, coins[6].z);
		glColor3f(0.0,0.0,0.0);
		coins[6].drawcoins();
	glPopMatrix();

}

void reset()
{
	strike.x = 0.0f;
	strike.y = -3.5f; 
	strike.ball_velx = 0.0f; 
	strike.ball_vely = 0.0f; 
   	tri_x = 0.0f;
   	tri_y = -3.5f; 
   	tri_z = 0.0f;
   	theta = 0.0f; 
   	tri_h = 1.0f;
   	flag_striker = 0;
}

//Functions required for OpenGL rendering
void initRendering(){
	// We enable 
	glEnable(GL_DEPTH_TEST);      // For 3D modelling (or for overlapping figures in 2D or 3D)
	glEnable(GL_COLOR_MATERIAL);  //Enable color
	glClearColor(1.0f, 1.0f, 0.8f, 1.0f); //Change the background to sky blue
}

//Called when the window is resized
void handleResize(int w, int h) 
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (double)w / (double)h, 0.1, 200.0);   // openGL renders the viecone using the coords 1.0 and 200.0
}


//Called to draw the scene
void draw(){
	B.drawScene();
}

void striker(int key, int x, int y) {

	if ((key == GLUT_KEY_LEFT) && (strike.x >= -3.4f) && (flag_striker == 0) && (flag_initialize == 1))
	{
		strike.x -= 0.1;
		tri_x -= 0.1;
	}
	if ((key == GLUT_KEY_RIGHT) && (strike.x <= 3.4f) && (flag_striker == 0) && (flag_initialize == 1))
	{
		strike.x += 0.1;
		tri_x += 0.1;
	}

	if ((key == GLUT_KEY_UP) && (tri_h <= 20.0f) && (flag_striker == 0) && (flag_initialize == 1))
		tri_h += 0.3f;
	if ((key == GLUT_KEY_DOWN) && (tri_h >= 1.0f) && (flag_striker == 0) && (flag_initialize == 1))
		tri_h -= 0.3f;

}

void score_set(int value)
{   
	timer_s += 1;
   	if(B.player==1)
		B.score1 -= 1;
	else if(B.player==2)
		B.score2 -= 1;
		
	glutTimerFunc(1000, score_set, 0);
}

void keyboardpress(unsigned char key, int x, int y) 
{
	float strike_power = tri_h/80;
	if((key == 'a') && (flag_striker == 0) && (flag_initialize == 1))   
  		theta += 2.0f;

	if((key == 'c') && (flag_striker == 0) && (flag_initialize == 1))
		theta -= 2.0f;

	if((key == 'w') && flag_initialize == 0)
	{
		B.choice = 'w';
		flag_initialize = 1;
		glutTimerFunc(1000, score_set, 0);
	}
	if((key == 'b') && flag_initialize == 0)
	{
		B.choice = 'b';
		flag_initialize = 1;
		glutTimerFunc(1000, score_set, 0);
	}

	if((key == 32) && (flag_striker == 0) && (flag_initialize == 1)) 
	{
		tri_z = 202.0f;
		flag_striker = 1;
		strike.set_velx(strike_power*sin(DEG2RAD(-theta)));
		strike.set_vely(strike_power*cos(DEG2RAD(-theta)));
	}

	if((key == 13) && (flag_striker == 1) && (flag_initialize == 1))
	{	
		flag_striker = 0;
		reset();
		if(B.player==1)
		{
			B.player=2;	
		}
		else if(B.player==2)
		{
			B.player=1;
		}
	}

	if(key == 27)
		exit(0);		
}

void coinscore(int i){
	if(B.player==1)
		B.score1 += coins[i].points1;

	else if(B.player==2)		
		B.score2 += coins[i].points2;
}

void resetcoin(int i)
{
	coinscore(i);
	coins[i].x = 202.0f;
	coins[i].y = 202.0f;
	coins[i].z = 202.0f;
	
}

void collision(int i, int j)
{
	float dx,dy,dist,xvel,yvel,dot,xcol,ycol,colweightS,colweightQ,colscale,combmass;
	dx = coins[i].x - coins[j].x;
	dy = coins[i].y - coins[j].y;
	dist = dx*dx + dy*dy;
	if( dist <= (coins[j].ball_rad+coins[i].ball_rad)*(coins[j].ball_rad+coins[i].ball_rad))
	{
		xvel = coins[j].ball_velx - coins[i].ball_velx;
		yvel = coins[j].ball_vely - coins[i].ball_vely;
		dot = dx*xvel + dy*yvel;
		if(dot > 0)
		{
			colscale = dot/dist;
			xcol = dx*colscale;
			ycol = dy*colscale;
			combmass = 2;
			colweightQ = 2*1/combmass;
			colweightS = 2*1/combmass;
			coins[i].ball_velx += colweightQ * xcol;
			coins[i].ball_vely += colweightQ * ycol;
			coins[j].ball_velx -= colweightS * xcol;
			coins[j].ball_vely -= colweightS * ycol;
		}
	}
}

void update(int value) 
{
	float strike_power = tri_h/80;

	float temp = 1 - (0.003/strike_power);
	float dx,dy,dist,xvel,yvel,dot,colscale,xcol,ycol,combmass,colweightQ,colweightS;
	int i,k;
    // Handle ball collisions with box
    if(strike.x + strike.ball_rad > 5 || strike.x - strike.ball_rad < -5)
        strike.ball_velx *= -1;
    if(strike.y + strike.ball_rad > 5 || strike.y - strike.ball_rad < -5)
        strike.ball_vely *= -1;

    for(i=0;i<7;i++)
    {
    	if(coins[i].x + coins[i].ball_rad > 5 || coins[i].x - coins[i].ball_rad < -5)
    	    coins[i].ball_velx *= -1;
    	if(coins[i].y + coins[i].ball_rad > 5 || coins[i].y - coins[i].ball_rad < -5)
    	    coins[i].ball_vely *= -1;

		dx = coins[i].x - strike.x;
		dy = coins[i].y - strike.y;
		dist = dx*dx + dy*dy;
	
		if( dist <= (strike.ball_rad+coins[i].ball_rad)*(strike.ball_rad+coins[i].ball_rad))
		{
			xvel = strike.ball_velx - coins[i].ball_velx;
			yvel = strike.ball_vely - coins[i].ball_vely;
			dot = dx*xvel + dy*yvel;
			if(dot > 0)
			{
				colscale = dot/dist;
				xcol = dx*colscale;
				ycol = dy*colscale;

				combmass = 3;
				colweightQ = 2*2/combmass;
				colweightS = 2*1/combmass;
				coins[i].ball_velx += colweightQ * xcol;
				coins[i].ball_vely += colweightQ * ycol;
				strike.ball_velx -= colweightS * xcol;
				strike.ball_vely -= colweightS * ycol;
			}
		}

		coins[i].ball_velx*=temp;
		coins[i].ball_vely*=temp;
    
        coins[i].x += coins[i].ball_velx;
   		coins[i].y += coins[i].ball_vely;

    	for(k=0;k<7;k++)
    	{
    		if(i!=k)
    			collision(i,k);
    	}

    	if( ((coins[i].x - pocket1.x)*(coins[i].x - pocket1.x) + (coins[i].y - pocket1.y)*(coins[i].y - pocket1.y)) < (pocket1.ball_rad * pocket1.ball_rad) )
    		resetcoin(i);
    	if( ((coins[i].x - pocket2.x)*(coins[i].x - pocket2.x) + (coins[i].y - pocket2.y)*(coins[i].y - pocket2.y)) < (pocket2.ball_rad * pocket2.ball_rad) )
    		resetcoin(i);
    	if( ((coins[i].x - pocket3.x)*(coins[i].x - pocket3.x) + (coins[i].y - pocket3.y)*(coins[i].y - pocket3.y)) < (pocket3.ball_rad * pocket3.ball_rad) )
    		resetcoin(i);
    	if( ((coins[i].x - pocket4.x)*(coins[i].x - pocket4.x) + (coins[i].y - pocket4.y)*(coins[i].y - pocket4.y)) < (pocket4.ball_rad * pocket4.ball_rad) )
    		resetcoin(i);
	}

	strike.ball_velx*=temp;
	strike.ball_vely*=temp;

	strike.x += strike.ball_velx;
    strike.y += strike.ball_vely;

    if( ((strike.x - pocket1.x)*(strike.x - pocket1.x) + (strike.y - pocket1.y)*(strike.y - pocket1.y)) < (pocket1.ball_rad * pocket1.ball_rad) )
    {	
    	if(B.player==1)
			B.score1 += strike.points1;
			
		else if(B.player==2)
			B.score2 += strike.points2;
    	reset(); 
    }
    if( ((strike.x - pocket2.x)*(strike.x - pocket2.x) + (strike.y - pocket2.y)*(strike.y - pocket2.y)) < (pocket2.ball_rad * pocket2.ball_rad) )
    {	
    	if(B.player==1)
			B.score1 += strike.points1;
		
		else if(B.player==2)
			B.score2 += strike.points2;
    	reset(); 
    }
    if( ((strike.x - pocket3.x)*(strike.x - pocket3.x) + (strike.y - pocket3.y)*(strike.y - pocket3.y)) < (pocket3.ball_rad * pocket3.ball_rad) )
    {	
    	if(B.player==1)
			B.score1 += strike.points1;
		
		else if(B.player==2)
			B.score2 += strike.points2;
		reset(); 
	}
    if( ((strike.x - pocket4.x)*(strike.x - pocket4.x) + (strike.y - pocket4.y)*(strike.y - pocket4.y)) < (pocket4.ball_rad * pocket4.ball_rad) )
    {	
    	if(B.player==1)
			B.score1 += strike.points1;
		
		else if(B.player==2)
			B.score2 += strike.points2;
		reset(); 
	}

    glutTimerFunc(10, update, 0);

}
/*
void mouseButton(int button, int state, int x, int y) {

	// only start motion if the left button is pressed
	if (button == GLUT_LEFT_BUTTON) 
	{
		// when the button is released
		if (state == GLUT_UP) 
		{
			
			int h=glutGet(GLUT_WINDOW_HEIGHT);
			int w=glutGet(GLUT_WINDOW_WIDTH);
			//float mouseX = ((float)x/(float)w);
			//float mouseY = ((float)y/(float)h); 
			cout<<"the x cord of mouse:"<<x<<endl;
			cout<<"the y cord of mouse:"<<y<<endl;
			cout<<"the h:"<<h<<endl;
			cout<<"the w:"<<w<<endl;
			//cout<<"the newx:"<<mouseX<<endl;
			//cout<<"the newy:"<<mouseY<<endl;

		}
	}
}
*/
int main(int argc, char** argv)
{
	
	coins[0].set_params(0.0f,0.0f,0.2,50,0,0,0);
	coins[1].set_params(0.0f,0.0f,0.2,0,0.8,0.5,0);
	coins[2].set_params(0.0f,0.0f,0.2,0,-0.8,0.5,0);
	coins[3].set_params(0.0f,0.0f,0.2,0,0,-0.95,0);
	coins[4].set_params(0.0f,0.0f,0.2,0,0.8,-0.5,0);
	coins[5].set_params(0.0f,0.0f,0.2,0,-0.8,-0.5,0);
	coins[6].set_params(0.0f,0.0f,0.2,0,0,0.95,0);

	strike.set_params(0.0f,0.0f,0.3,-5,0.0,-3.5,0);
    
    pocket1.set_params(0.0f,0.0f,0.4,0,-4.6f, 4.6f, 0.0f);
	pocket2.set_params(0.0f,0.0f,0.4,0,4.6f, 4.6f, 0.0f);
	pocket3.set_params(0.0f,0.0f,0.4,0,4.6f, -4.6f, 0.0f);
	pocket4.set_params(0.0f,0.0f,0.4,0,-4.6f, -4.6f, 0.0f);
	
	//Both the players start with 30 points
	B.score1=30;
	B.score2=30;
	//The First player is set 1. Second Player is 2.
	B.player=1;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB );  // GLUT_DOUBLE => Double buffer in our game
	glutInitWindowSize(1366, 768);

	//Create the window
	glutCreateWindow("Carrom Game in OpenGL/Glut");
	initRendering();
	
	//Set handler functions
	glutDisplayFunc(draw);
	glutIdleFunc(draw);
	glutReshapeFunc(handleResize);
	glutSpecialFunc(striker);
	glutKeyboardFunc(keyboardpress);
	//glutMouseFunc(mouseButton);
	//glutMotionFunc(mouseMove);
	glutTimerFunc(10, update, 0);
	// Continuously call the callback functions (mentioned above)
	glutMainLoop();
	return 0;
}